#!/usr/bin/python3

import argparse
import pathlib
import gitlab
import datetime

parser = argparse.ArgumentParser(description='Generate a changelog')
parser.add_argument('milestone', type=str, help='Milestone to generate changelog for')
parser.add_argument('--label', type=str, help='Label to filter the issues')
parser.add_argument('--project', type=str, metavar='PROJECT', action='append',
                    help='Project(s) concerned by the changelog')
parser.add_argument('--gitlab', type=str, metavar='SECTION',
                    help='Section to use from the config file')
parser.add_argument('--config-file', type=str, metavar='FILE', action='append',
                    help='Config file to use. Default is to look for /etc/python-gitlab.cfg or ~/.python-gitlab.cfg')
args = parser.parse_args()

print('## %"{0}" - {1}'.format(args.milestone, '{:%Y-%m-%d}'.format(datetime.date.today())))
print()

gl = gitlab.Gitlab.from_config(args.gitlab, args.config_file)

if (args.project):
  projects = []
  for pid in args.project:
    projects.append(gl.projects.get(pid))
else:
  projects = gl.projects.list(all=True)

def print_issues(title, label):
  title_printed = 0
  for project in projects:
    try:
      customLabel = [ args.label, label ]
      issues = project.issues.list(state='closed', scope='all', sort='asc', milestone=args.milestone, labels=customLabel, all=True)
    except:
      issues = project.issues.list(state='closed', scope='all', sort='asc', milestone=args.milestone, labels=[ label ], all=True)
    if (issues):
      if (title_printed == 0):
        print('### {0}'.format(title))
        print()
        title_printed = 1
      print('#### {0}'.format(project.name))
      for issue in issues:
        if ('Skip' in issue.labels):
          continue
        print("- {2}#{1} {0}".format(issue.title, issue.iid, project.path))
      print()
  return title_printed

#~ Added for new features.
print_issues('Added', 'Added')
#~ Changed for changes in existing functionality.
print_issues('Changed', 'Changed')
#~ Deprecated for soon-to-be removed features.
print_issues('Deprecated', 'Deprecated')
#~ Removed for now removed features.
print_issues('Removed', 'Removed')
#~ Fixed for any bug fixes.
print_issues('Fixed', 'Fixed')
#~ Security in case of vulnerabilities.
print_issues('Security', 'Security')
