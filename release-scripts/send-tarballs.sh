#!/bin/sh

REPO_DIR="/home/bmortier/tmp/"

###################### DON'T TOUCH AFTER THIS LINE ######################################

TARBALLS_DIR="fusiondirectory-tarballs"
DEPOT_SERVER="www.opensides.be:"

if [ -z "$1" ]; then
   echo "usage: $0 software version"
   exit
fi

if [ -z "$2" ]; then
   echo "usage: $0 software version"
   exit
fi

software=$1
version=$2

cd $REPO_DIR/$TARBALLS_DIR || exit

echo "sending the new release to the depot server"

scp CHECKSUM.MD5 CHECKSUM.MD5.asc www.opensides.be:
scp "$software-$version.tar.gz" "$software-$version.tar.gz.asc" $DEPOT_SERVER

if [ "$software" = 'fusiondirectory' ] ; then
  scp "$software-plugins-$version.tar.gz" "$software-plugins-$version.tar.gz.asc" $DEPOT_SERVER
fi

exit
