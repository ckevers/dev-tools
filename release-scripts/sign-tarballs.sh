#!/bin/sh

REPO_DIR="/home/bmortier/tmp/"
GPG_SIGNATURE="0xFE0FEAE5AC483A86"

###################### DON'T TOUCH AFTER THIS LINE ######################################

TARBALLS_DIR="fusiondirectory-tarballs"

if [ -z "$1" ]; then
   echo "usage: $0 software version"
   exit
fi

if [ -z "$2" ]; then
   echo "usage: $0 software version"
   exit
fi

software=$1
version=$2

cd $REPO_DIR || exit

if [ "$software" = 'fusiondirectory' ] ; then

echo "getting the old releases from official depot"
rsync -avz www.opensides.be:/var/www/vhosts/fd-repos/sources/fusiondirectory/ $TARBALLS_DIR

echo "getting the new releases from the jenkins"

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/sources/fusiondirectory/"$software-$version.tar.gz" fusiondirectory-tarballs/

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/sources/fusiondirectory/"$software-plugins-$version.tar.gz" fusiondirectory-tarballs/

fi 

if [ "$software" = 'argonaut' ] ; then
echo "getting the old releases from official depot"

rsync -avz www.opensides.be:/var/www/vhosts/fd-repos/sources/argonaut/ $TARBALLS_DIR

echo "getting the new releases from the jenkins"

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/sources/argonaut/"$software-$version.tar.gz" fusiondirectory-tarballs/

fi 

cd $TARBALLS_DIR || exit

echo "signing the new release with the gpg key from fusiondirectory"

gpg --armor --detach-sign --default-key $GPG_SIGNATURE "$software-$version.tar.gz"

if [ "$software" = 'fusiondirectory' ] ; then
  gpg --armor --detach-sign --default-key $GPG_SIGNATURE "$software-plugins-$version.tar.gz"
fi

echo "generating a new CHECKSUM.MD5 file and signing with the gpg key from fusiondirectory"

md5sum -- *.tar.gz > CHECKSUM.MD5

gpg --armor --detach-sign --default-key $GPG_SIGNATURE CHECKSUM.MD5

exit
