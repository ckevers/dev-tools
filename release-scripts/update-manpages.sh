#!/bin/sh

REPO_DIR="/home/bmortier/develop/projects/fusiondirectory/"
REPO_DIR_ARGONAUT="/home/bmortier/develop/projects/argonaut/"

###################### DON'T TOUCH AFTER THIS LINE ######################################

ARGONAUT_DIR="argonaut"
ARGONAUT="Argonaut"

FD_DIR="fusiondirectory"
FD_PLUGINS_DIR="fusiondirectory-plugins"
FD="FusionDirectory"

if [ -z "$1" ]; then
   echo "usage: $0 software version"
   exit
fi

if [ -z "$2" ]; then
   echo "usage: $0 software version"
   exit
fi

software=$1
version=$2

if [ "$software" = 'fusiondirectory' ] ; then

cd $REPO_DIR || exit

cd $FD_DIR || exit

echo "building new manpages"

pod2man -c "$FD Documentation" -r "$FD $version" contrib/bin/fusiondirectory-insert-schema contrib/man/fusiondirectory-insert-schema.1
pod2man -c "$FD Documentation" -r "$FD $version" contrib/bin/fusiondirectory-setup contrib/man/fusiondirectory-setup.1
pod2man -c "$FD Documentation" -r "$FD $version" contrib/man/fusiondirectory.conf.pod contrib/man/fusiondirectory.conf.5

fi 

if [ "$software" = 'argonaut' ] ; then

cd $REPO_DIR_ARGONAUT || exit

cd $ARGONAUT_DIR || exit

echo "building new manpages"

pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-client/bin/argonaut-client argonaut-client/man/argonaut-client.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-common/man/argonaut.conf.pod argonaut-common/man/argonaut.conf.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-mirror/bin/argonaut-debconf-crawler  argonaut-fai-mirror/man/argonaut-debconf-crawler.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-mirror/bin/argonaut-repository argonaut-fai-mirror/man/argonaut-repository.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-monitor/bin/argonaut-fai-monitor argonaut-fai-monitor/man/argonaut-fai-monitor.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-nfsroot/bin/argonaut-ldap2fai  argonaut-fai-nfsroot/man/argonaut-ldap2fai.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-server/bin/fai2ldif argonaut-fai-server/man/fai2ldif.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fai-server/bin/yumgroup2yumi argonaut-fai-server/man/yumgroup2yumi.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fuse/bin/argonaut-fuse  argonaut-fuse/man/argonaut-fuse.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fusiondirectory/bin/argonaut-clean-audit argonaut-fusiondirectory/man/argonaut-clean-audit.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-fusiondirectory/bin/argonaut-user-reminder argonaut-fusiondirectory/man/argonaut-user-reminder.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-ldap2zone/bin/argonaut-ldap2zone  argonaut-ldap2zone/man/argonaut-ldap2zone.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-quota/bin/argonaut-quota argonaut-quota/man/argonaut-quota.1
pod2man -c "$ARGONAUT Documentation" -r "$ARGONAUT $version" argonaut-server/bin/argonaut-server argonaut-server/man/argonaut-server.1

fi 

exit
