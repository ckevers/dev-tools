#!/bin/sh

REPO_DIR="/home/bmortier/tmp/"
GPG_SIGNATURE="0xFE0FEAE5AC483A86"

###################### DON'T TOUCH AFTER THIS LINE ######################################

PKG_DIR="fusiondirectory-repositories"
FD_DIR="fusiondirectory"
ARGONAUT_DIR="argonaut"
SCHEMA2LDIF_DIR="schema2ldif"
DEBIAN_DIR="debian"
RPM_DIR="rpm"

if [ -z "$1" ]; then
   echo "usage: $0 software version (in short form ex: 1.2.1 become 121)"
   exit
fi

if [ -z "$2" ]; then
   echo "usage: $0 software version (in short form ex: 1.2.1 become 121)"
   exit
fi

software=$1
version=$2

# create debian dir inside fusiondirectory

mkdir -p $REPO_DIR/$PKG_DIR/$FD_DIR/$DEBIAN_DIR
mkdir -p $REPO_DIR/$PKG_DIR/$FD_DIR/$RPM_DIR

# create debian dir inside argonaut

mkdir -p $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$DEBIAN_DIR
mkdir -p $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$RPM_DIR

# create debian dir inside schema2ldif

mkdir -p $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$DEBIAN_DIR
mkdir -p $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$RPM_DIR

# get the official deb for fusiondirectory

if [ "$software" = 'schema2ldif' ] ; then

echo "getting the official releases from the jenkins"

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/schema2ldif-"$version"-stretch/ $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$DEBIAN_DIR/stretch

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/schema2ldif-"$version"-jessie/ $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$DEBIAN_DIR/jessie

# signature of debian repositories

cd $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$DEBIAN_DIR/stretch || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export stretch

cd $REPO_DIR/$PKG_DIR/$SCHEMA2LDIF_DIR/$DEBIAN_DIR/jessie || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export jessie

fi 


# get the official rpm and deb for fusiondirectory

if [ "$software" = 'fusiondirectory' ] ; then

echo "getting the official releases from the jenkins"

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/fusiondirectory-"$version"-stretch/ $REPO_DIR/$PKG_DIR/$FD_DIR/$DEBIAN_DIR/stretch

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/fusiondirectory-"$version"-jessie/ $REPO_DIR/$PKG_DIR/$FD_DIR/$DEBIAN_DIR/jessie

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/rpm/rhel/7/fusiondirectory-"$version"/ $REPO_DIR/$PKG_DIR/$FD_DIR/$RPM_DIR/7

# signature of debian repositories

cd $REPO_DIR/$PKG_DIR/$FD_DIR/$DEBIAN_DIR/stretch || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export stretch

cd $REPO_DIR/$PKG_DIR/$FD_DIR/$DEBIAN_DIR/jessie || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export jessie

# signature of rpm repositories for rhel 7

cd $REPO_DIR/$PKG_DIR/$FD_DIR/$RPM_DIR/7/RPMS || exit

rpmsign --addsign --key-id=$GPG_SIGNATURE ./*.rpm

createrepo .

cd $REPO_DIR/$PKG_DIR/$FD_DIR/$RPM_DIR/7/SRPMS || exit

rpmsign --addsign --key-id=$GPG_SIGNATURE ./*.rpm

createrepo .

fi 

if [ "$software" = 'argonaut' ] ; then

echo "getting the official releases from the jenkins"

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/argonaut-stretch/ $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$DEBIAN_DIR/stretch

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/debian/argonaut-jessie/ $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$DEBIAN_DIR/jessie

rsync -avz integration.fusiondirectory.org:/var/www/repos/official-releases/rpm/rhel/7/argonaut/ $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$RPM_DIR/7

# signature of debian repositories

cd $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$DEBIAN_DIR/stretch || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export stretch

cd $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$DEBIAN_DIR/jessie || exit

echo "SignWith: " $GPG_SIGNATURE >> conf/distributions

reprepro -vvv export jessie

# signature of rpm repositories for rhel 7

cd $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$RPM_DIR/7/RPMS || exit

rpmsign --addsign --key-id=$GPG_SIGNATURE ./*.rpm

createrepo .

cd $REPO_DIR/$PKG_DIR/$ARGONAUT_DIR/$RPM_DIR/7/SRPMS || exit

rpmsign --addsign --key-id=$GPG_SIGNATURE ./*.rpm

createrepo .

fi 

exit

